
#include <iostream>
using namespace std;

class Fraction
{
public:
	Fraction()
	{
		Numerator = 5;
		Denominator = 10;
	}
	
	Fraction(int InputNumerator, int  InputDenominator)
	{
		if(InputDenominator <= 0 )
		{
			throw runtime_error("an error occurred when creating the object. 0 is not a valid number");
		}

		Numerator = InputNumerator;
		Denominator = InputDenominator;
	}
	
private:
	int Numerator;
	int Denominator;
};

int main()
{
	int a, b;
	cout << "Enter a two nubmer" << endl;

	try
	{
		cin >> a >> b;
		
		if (b <= 0)
		{
			throw exception("0 is not a valid number");
		}

		Fraction fraction(a, b);	
	}
	catch (const runtime_error& Error)
	{
		cout << Error.what();
	}
	catch (const exception& Message)
	{
		cout << Message.what();
	}
}
